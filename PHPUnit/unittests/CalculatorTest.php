<?php


use App\Calculator;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{

    public function testDivide()
    {
        $calculator = new App\Calculator();
        $result = $calculator->divide(20,5);

        $this->assertEquals(4, $result);
    }

    public function testSubtract()
    {
        $calculator = new App\Calculator();
        $result = $calculator->subtract(20,5);

        $this->assertequals(15, $result);
    }

    public function testMultiply()
    {
        $calculator = new App\Calculator();
        $result = $calculator->multiply(20,5);

        $this->assertequals(100, $result);
    }

    public function testUadd()
    {
        $calculator = new App\Calculator();
        $result = $calculator->Uadd(20,5);

        $this->assertequals(25, $result);
    }
}
