
Feature: Add to Cart
    @TEST_QAT-233
    Scenario: Add items to cart
    Given On website homepage
        When Click on Collection
        And Click on Product
        And Click add to cart on an item
        Then You have an item in your cart
    