
Feature: Add items to wishlist
  @TEST_QAT-231
  Scenario: Adding items to wishlist
    Given Logged in to website as a customer
    When Click on Collection
    And Click on Product
    And Click on heart
    And Add item to collection
    Then Item is added to wishlist