
Feature: Add Address To Account
  @TEST_QAT-230
  Scenario: Adding first address to account
    Given Logged in to website as a customer
    And Account has no address
    When Click on Addresses
    And Add a new address
    Then Address is added to account