@TEST_QAT-234
Feature: Check Out Using Cards

  Background:
    Given On website homepage
    And Go to Full Screen
    And Customer Logged in
    And Click on Start Shopping
    And Click add to cart on three items
    And You have an Items in your cart

  Scenario: Checking out using Visa
    Given You have an Items in your cart
    When Click on Checkout
    And Adding Shipping Address
    And Click Heavy Goods Shipping
    And Fill in Visa information
    Then You have Checked Out

  Scenario: Checking out using Mastercard
    Given You have an Items in your cart
    When Click on Checkout
    And Adding Shipping Address
    And Click Heavy Goods Shipping
    And Fill in Mastercard information
    Then You have Checked Out

  Scenario: Checking out using American Express
    Given You have an Items in your cart
    When Click on Checkout
    And Adding Shipping Address
    And Click Heavy Goods Shipping
    And Fill in American Express information
    Then You have Checked Out

  Scenario: Checking out using Discover
    Given You have an Items in your cart
    When Click on Checkout
    And Adding Shipping Address
    And Click Heavy Goods Shipping
    And Fill in Discover information
    Then You have Checked Out

  Scenario: Checking out using Diners Club
    Given You have an Items in your cart
    When Click on Checkout
    And Adding Shipping Address
    And Click Heavy Goods Shipping
    And Fill in Diners Club information
    Then You have Checked Out

  Scenario: Checking out using JCB
    Given You have an Items in your cart
    When Click on Checkout
    And Adding Shipping Address
    And Click Heavy Goods Shipping
    And Fill in JCB information
    Then You have Checked Out