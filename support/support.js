const { setWorldConstructor } = require('@cucumber/cucumber')
 
const { seleniumWebdriver } = require('selenium-webdriver');
 
var firefox = require('selenium-webdriver/firefox');
 
var chrome = require('selenium-webdriver/chrome');
 
class CustomWorld {
  constructor() {
    this.variable = 0
  }


  CustomWorld() {

    this.driver = new seleniumWebdriver.Builder()
        .forBrowser('chrome')
        .build();
  }

  setWorldConstructor(CustomWorld) {

    module.exports = function () {

      this.World = CustomWorld;

      this.setDefaultTimeout(3 * 1000);
    }
  };
}