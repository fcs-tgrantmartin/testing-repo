const {After} = require('@cucumber/cucumber');

After(function() {
      if(this.driver !=null){
        return this.driver.quit();
      }
  });