const {Given, When, Then} = require('@cucumber/cucumber');
const assert = require('assert');
const faker = require('faker');

//Customer Log in functions

Given('On website homepage', async function () {
    await this.homePage.load();
});

When('Fill in valid account information and click sign in', async function() {
    await this.homePage.authenticate('jim@starfleet.com', 'StarTrek5');
});

Then('User Account page is displayed', async function () {
    assert(await this.homePage.waitForTitle('Account'));
});

//Customer Log Out functions

Given('Logged in to website as a customer', async function () {
    await this.homePage.load();
    await this.homePage.authenticate('jim@starfleet.com', 'StarTrek5');
    await this.homePage.logoClick();
});

When('Click on Logout', async function () {
    await this.homePage.logout();
});

Then('Logged out user homepage is displayed', async function () {
    assert.strictEqual(this.homePage.formText(), 'Login/Signup');
});

//Create New Account functions
When('Click on Login form', async function () {
    await this.homePage.formClick();
});

When('Click on Create your account', async function () {
    await this.homePage.createAccount();
});

When('Fill in sign up form and click sign up', async function () {
    await this.homePage.newAccount(
        faker.fake("{{name.firstName}}"),
        faker.fake("{{name.lastName}}"),
        faker.fake("{{internet.email}}"),
        faker.fake("{{internet.password}}")
    );
});

//Account Address functions
Given('Account has no address', async function() {
    let number = await this.accountPage.addressNumber();
    if (number !== 0)
        return 'skipped';
});

Given('Account has more than one Address', async function() {
    let number = this.accountPage.addressNumber();
    if (number <= 1)
        return 'skipped';
});

When('Click on Addresses', async function () {
    await this.accountPage.addressLink();
});

When('Add a new address', async function () {
    await this.accountPage.addAddress(
        faker.fake("{{name.firstName}}"),
        faker.fake("{{name.lastName}}"),
        faker.fake("{{company.companyName}}"),
        faker.fake("{{address.streetAddress}}"),
        faker.fake("{{address.city}}"),
        faker.fake("{{address.zipCode}}")
    );
});

When('Click Delete Address', async function () {
    await this.accountPage.deleteAddress();
    await assert.strictEqual(this.accountPage.alertText(), 'Are you sure you wish to delete this address?');
    await alert.accept();
});

Then('Address is added to account', async function () {
    assert(this.accountPage.addressExists());
});

Then('Address is Removed', async function () {
    assert.strictEqual(this.accountPage.addressNumber(), "1");
});