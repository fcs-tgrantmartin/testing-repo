const webdriver = require('selenium-webdriver');
const {When, Then} = require('@cucumber/cucumber');
const faker = require('faker');
const assert = require("assert");

//Store navigation functions
When('Click on Collection', async function () {
    await this.homePage.collection1();
});

When('Click on Product', async function () {
    await this.collectionPage.product1();
});

//Wishlist functions
When('Click on heart', async function () {
    await this.productPage.favorite();
});

Then('Item is added to wishlist', async function () {
    await this.driver.findElement(webdriver.By.xpath("//*[@id=\"shopify-section-header\"]/section/header/div/div/div[3]/div[3]/span/a/img")).click();

    if (await this.driver.findElement(webdriver.By.xpath("//*[@id=\"swym-tab-modal\"]/div/div")))
        await this.driver.findElement(webdriver.By.xpath("//*[@id=\"swym-welcome-button\"]")).click();

    if(await this.driver.findElement(webdriver.By.xpath("//*[@id=\"temp_1620147151880\"]/div")))
        return true;
});


//Shopping Cart functions
When('Click add to cart on an item', async function () {
    await this.productPage.addToCart(1);
});

Then('You have an item in your cart', async function () {
    let title = await this.driver.findElement(webdriver.By.xpath("//*[@id=\"shopify-section-header\"]/section/header/div/div/div[3]/div[4]/a/div/div/span[1]/span")).getText();
    assert(title > 0);
});

When('Click on Checkout', async function () {
    await this.checkout();
});

//Checkout and Shipping functions
When('Adding Shipping Address', async function () {
    await this.driver.findElement(webdriver.By.id("checkout_shipping_address_first_name")).sendKeys(faker.fake("{{name.firstName}}"));
    await this.driver.findElement(webdriver.By.id("checkout_shipping_address_last_name")).sendKeys(faker.fake("{{name.lastName"));
    await this.driver.findElement(webdriver.By.id("checkout_shipping_address_company")).sendKeys(faker.fake("{{company.companyName}}"));
    await this.driver.findElement(webdriver.By.id("checkout_shipping_address_address1")).sendKeys(faker.fake("{{address.streetAddress}}"));
    await this.driver.findElement(webdriver.By.id("checkout_shipping_address_city")).sendKeys(faker.fake("{{address.city}}"));
    await this.driver.findElement(webdriver.By.id("checkout_shipping_address_zip")).sendKeys(faker.fake("{{address.zipCode}}"));
    await this.driver.findElement(webdriver.By.id("checkout_shipping_address_phone")).sendKeys(faker.fake("{{phone.phoneNumber}}"));
    await this.driver.findElement(webdriver.By.id("continue_button")).click();
});

When('Click Heavy Goods Shipping', async function () {
    await this.driver.findElement(webdriver.By.id("checkout_shipping_rate_id_shopify-heavy20goods20shipping-0_00")).click();
    await this.driver.findElement(webdriver.By.id("continue_button")).click();
});

When('Fill in Visa information', async function () {
    await this.driver.findElement(webdriver.By.id("number")).sendKeys("4242424242424242");
    await this.driver.findElement(webdriver.By.id("name")).sendKeys("Grant Skywalker");
    await this.driver.findElement(webdriver.By.id("expiry")).sendKeys("0828");
    await this.driver.findElement(webdriver.By.id("verification_value")).sendKeys("333");
    await this.driver.findElement(webdriver.By.id("continue_button")).click();
});

When('Fill in Mastercard information', async function () {
    await this.driver.findElement(webdriver.By.id("number")).sendKeys("5555555555554444");
    await this.driver.findElement(webdriver.By.id("name")).sendKeys("Grant Skywalker");
    await this.driver.findElement(webdriver.By.id("expiry")).sendKeys("0828");
    await this.driver.findElement(webdriver.By.id("verification_value")).sendKeys("333");
    await this.driver.findElement(webdriver.By.id("continue_button")).click();
});

When('Fill in American Express information', async function () {
    await this.driver.findElement(webdriver.By.id("number")).sendKeys("378282246310005");
    await this.driver.findElement(webdriver.By.id("name")).sendKeys("Grant Skywalker");
    await this.driver.findElement(webdriver.By.id("expiry")).sendKeys("0828");
    await this.driver.findElement(webdriver.By.id("verification_value")).sendKeys("333");
    await this.driver.findElement(webdriver.By.id("continue_button")).click();
});

When('Fill in Discover information', async function () {
    await this.driver.findElement(webdriver.By.id("number")).sendKeys("6011111111111117");
    await this.driver.findElement(webdriver.By.id("name")).sendKeys("Grant Skywalker");
    await this.driver.findElement(webdriver.By.id("expiry")).sendKeys("0828");
    await this.driver.findElement(webdriver.By.id("verification_value")).sendKeys("333");
    await this.driver.findElement(webdriver.By.id("continue_button")).click();
});

When('Fill in Diners Club information', async function () {
    await this.driver.findElement(webdriver.By.id("number")).sendKeys("30569309025904");
    await this.driver.findElement(webdriver.By.id("name")).sendKeys("Grant Skywalker");
    await this.driver.findElement(webdriver.By.id("expiry")).sendKeys("0828");
    await this.driver.findElement(webdriver.By.id("verification_value")).sendKeys("333");
    await this.driver.findElement(webdriver.By.id("continue_button")).click();
});

When('Fill in JCB information', async function () {
    await this.driver.findElement(webdriver.By.id("number")).sendKeys("3530111333300000");
    await this.driver.findElement(webdriver.By.id("name")).sendKeys("Grant Skywalker");
    await this.driver.findElement(webdriver.By.id("expiry")).sendKeys("0828");
    await this.driver.findElement(webdriver.By.id("verification_value")).sendKeys("333");
    await this.driver.findElement(webdriver.By.id("continue_button")).click();
});
