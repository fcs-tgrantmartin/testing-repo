const webdriver = require('selenium-webdriver');
const {setWorldConstructor, setDefaultTimeout} = require('@cucumber/cucumber');
const HomePage = require("../page_objects/home.page");
const AccountPage = require("../page_objects/account.page");
const CollectionPage = require("../page_objects/collection.page");
const ProductPage = require("../page_objects/product.page");
const CartPage = require("../page_objects/cart.page");

function CBTWorld() {
    const remoteHub = 'http://hub.crossbrowsertesting.com:80/wd/hub';

    const CBT_USERNAME = process.env.CBT_USERNAME;
    const CBT_AUTHKEY = process.env.CBT_AUTHKEY;
    const BROWSER_KEY = process.env.BROWSER_KEY;
    
    const browsers = {
       windows_chrome: {
           browserName: 'Chrome', platform: 'Windows 10', version: 'latest', screen_resolution: '1920x1080' },
       mac_chrome: { 
           browserName: 'Chrome', platform: 'Mac OSX 10.14', version: 'latest', screen_resolution: '1366x768' },
       windows_ie: {
           browserName: 'Internet Explorer', platform: 'Windows 8.1', version: '11', screen_resolution: '1366x768' },
       windows_firefox: {
           browserName: 'Firefox', platform: 'Windows 10', version: 'latest', screen_resolution: '1366x768' },
        mac_firefox: {
            browserName: 'Firefox', platform: 'Windows 10', version: 'latest', screen_resolution: '1366x768' },
        mac_safari: {
            browserName: 'Safari', platform: 'Mac OSX 11.0', version: '14', screen_resolution: '1366x768' }
    };

    let caps = {
        name : 'Cucumber Test',
        build : '1.0',
        browserName : browsers[BROWSER_KEY].browserName,
        platform : browsers[BROWSER_KEY].platform,
        screen_resolution : browsers[BROWSER_KEY].screen_resolution,
        record_video : 'true',
        username : CBT_USERNAME,
        password : CBT_AUTHKEY
    };

    this.driver = new webdriver.Builder()
        .usingServer(remoteHub)
        .withCapabilities(caps)
        .build();

    this.homePage = new HomePage(this.driver);
    this.accountPage = new AccountPage(this.driver);
    this.collectionPage = new CollectionPage(this.driver);
    this.productPage = new ProductPage(this.driver);
    this.cartPage = new CartPage(this.driver);

}

setDefaultTimeout(60 * 1000);
setWorldConstructor(CBTWorld);