
Feature: Logout Account
    @TEST_QAT-236
    Scenario: Logout Account
        Given Logged in to website as a customer
        When Click on Logout
        Then Logged out user homepage is displayed