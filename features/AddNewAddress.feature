
Feature: Add New Address To Account
  @TEST_QAT-232
  Scenario: Adding new address to account
    Given Logged in to website as a customer
    When Click on Addresses
    And Add a new address
    Then Address is added to account