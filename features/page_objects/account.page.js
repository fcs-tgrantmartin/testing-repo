//Logged in user account page
const LoginPage = require('./login.page')

const PAGE_URL = 'https://www.bluesummitsupplies.com/account';
const PAGE_TITLE = 'Account';
const ADDRESS_NUMBER = {xpath: '//*[@id="shopify-section-account-side-menu"]/div[1]/div/div/ul/li[5]/span'};
const ADDRESSES_LINK = {linkText: 'Addresses'};
const ADDRESSES_TITLE = 'Addresses';
const DELETE_ADDRESS = {xpath: '//*[@id="main"]/section/div/div/div/div[2]/div[1]/div[1]/div[2]/div/div/div/button[2]'};
const ADDRESS_DETAIL = {className: 'address-detail'};
const NEW_ADDRESS_FIRSTNAME = {id: 'address-new[first_name]'};
const NEW_ADDRESS_LASTNAME = {id: 'address-new[last_name]'};
const NEW_ADDRESS_BUSINESS = {id: 'address-new[company]'};
const NEW_ADDRESS_STREET = {id: 'address-new[address1]'};
const NEW_ADDRESS_CITY = {id: 'address-new[city]'};
//const NEW_ADDRESS_STATE = {};
const NEW_ADDRESS_ZIP = {id: 'address-new[zip]'};
const NEW_ADDRESS_BUTTON = {xpath: '//*[@id="address_form_new"]/div[8]/button'};

class AccountPage extends LoginPage {

    constructor(driver) {
        super(driver);
    }

    async load(){
        await this.driver.get(PAGE_URL);
        await this.waitForTitle(PAGE_TITLE);
    }

    async addressNumber(){
        return await this.text(ADDRESS_NUMBER);
    }

    async addressLink() {
        await this.click(ADDRESSES_LINK);
        return await this.waitForTitle(ADDRESSES_TITLE);
    }

    async deleteAddress() {
        await this.click(DELETE_ADDRESS);
    }

    async addressExists() {
        return this.waitUntilDisplayed(ADDRESS_DETAIL);
    }

    async addAddress(fname, lname, company, street, city, zip) {
        await this.type(NEW_ADDRESS_FIRSTNAME, fname);
        await this.type(NEW_ADDRESS_LASTNAME, lname);
        await this.type(NEW_ADDRESS_BUSINESS, company);
        await this.type(NEW_ADDRESS_STREET, street);
        await this.type(NEW_ADDRESS_CITY, city);
        await this.type(NEW_ADDRESS_ZIP, zip);
        return await this.click(NEW_ADDRESS_BUTTON);

    }



}

module.exports = AccountPage