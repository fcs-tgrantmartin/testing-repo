const LoginPage = require('./login.page')

const CHECKOUT_BUTTON = {xpath: '//*[@id="shopify-section-cart-template"]/section/div/div/div/div/form/div/div[1]/div/div[3]/button'}
const CART_QTY_UP_BUTTON = {xpath:'//*[@id="shopify-section-cart-template"]/section/div/div/div/div/div[1]/div/table/tbody/tr/td[2]/div/button[2]'}
const CART_QTY_DOWN_BUTTON = {xpath:'//*[@id="shopify-section-cart-template"]/section/div/div/div/div/div[1]/div/table/tbody/tr/td[2]/div/button[1]'}
const REMOVE_FROM_CART_BUTTON = {xpath:'//*[@id="shopify-section-cart-template"]/section/div/div/div/div/div[1]/div/table/tbody/tr/td[2]/a'}

class CartPage extends LoginPage {

    constructor(driver) {
        super(driver);
    }

    async checkout() {
        await this.click(CHECKOUT_BUTTON);
    }

    async removeFromCart() {
        await this.click(REMOVE_FROM_CART_BUTTON);
    }

    async cartQtyIncrease() {
        await this.click(CART_QTY_UP_BUTTON);
    }

    async cartQtyDecrease() {
        await this.click(CART_QTY_DOWN_BUTTON);
    }

}

module.exports = CartPage