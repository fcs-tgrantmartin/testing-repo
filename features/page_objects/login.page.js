//Base page attributes for any page where you can log in or out
const Page = require('./page')

const EMAIL_INPUT = { id: 'login-customer[email]' };
const PASSWORD_INPUT = { id: 'login-customer[password]' };
const LOGIN_BUTTON = { xpath: '//*[@id="header_customer_login"]/button' };
const LOGOUT_BUTTON = { xpath: '//*[@id="account-popover"]/div/div/a[6]' };
const CREATE_ACCOUNT_LINK = { xpath: '//*[@id="header-login-panel"]/div/div/p/button' };
const LOGIN_FORM_LINK = { xpath: '//*[@id="shopify-section-header"]/section/header/div/div/div[2]/div[2]/div/span'};
const REGISTER_FIRSTNAME = {id: 'b2c_register_customer_first_name'};
const REGISTER_LASTNAME = {id: 'b2c_register_customer_last_name'};
const REGISTER_EMAIL = {id: 'b2c_register_customer_email'};
const REGISTER_PASSWORD = {id: 'b2c_register_customer_password'};
const REGISTER_BUTTON = {xpath: '//*[@id="create_customer"]/button'};
const LOGGED_IN_TITLE = 'Account';


class LoginPage extends Page {

    constructor(driver) {
        super(driver);
    }

    async authenticate(email, password) {
        await this.waitUntilDisplayed(LOGIN_FORM_LINK);
        await this.click(LOGIN_FORM_LINK);
        await this.type(EMAIL_INPUT, email);
        await this.type(PASSWORD_INPUT, password);
        await this.click(LOGIN_BUTTON);
        await this.waitForTitle(LOGGED_IN_TITLE);
    }

    async formText() {
        return this.text(LOGIN_FORM_LINK);
    }

    async formClick() {
        return await this.click(LOGIN_FORM_LINK);
    }

    async logout() {
        await this.click(LOGIN_FORM_LINK);
        await this.click(LOGOUT_BUTTON);
    }

    async createAccount() {
        return this.click(CREATE_ACCOUNT_LINK);
    }

    async newAccount(fname, lname, email, password) {
        await this.type(REGISTER_FIRSTNAME, fname);
        await this.type(REGISTER_LASTNAME, lname);
        await this.type(REGISTER_EMAIL, email);
        await this.type(REGISTER_PASSWORD, password);
        return await this.click(REGISTER_BUTTON);
    }
}

module.exports = LoginPage