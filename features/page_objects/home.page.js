const LoginPage = require('./login.page')

const PAGE_URL = 'https://www.bluesummitsupplies.com'
const PAGE_TITLE = 'Blue Summit Supplies'
const FOLDERS_COLLECTION = {xpath: '//*[@id="block-160382294788d05ca4-0"]/a'}
const ENVELOPES_COLLECTION = {xpath:'//*[@id="block-160382294788d05ca4-1"]/a'}
const INK_AND_TONER_COLLECTION = {xpath:'//*[@id="block-160382294788d05ca4-2"]/a'}
const ALL_COLLECTIONS ={xpath:'//*[@id="shopify-section-collection-list"]/section/div/header/a'}
const LL_POPUP = {xpath: '//*[@id="lion-loyalty-panel-custom-css"]/div[5]'}
const LL_CLOSE = {xpath: '//*[@id="lion-loyalty-panel-custom-css"]/div[5]/div/a'}
const LOGO_LINK = {xpath: '//*[@id="shopify-section-header"]/section/header/div/div/h1/a'}

class HomePage extends LoginPage {

    constructor(driver) {
        super(driver)
    }

    async load() {
        await this.open(PAGE_URL);
        await this.waitForTitle(PAGE_TITLE);
        await this.loyaltyClose();
        return await this.maximize();
    }

    async collection1() {
        return await this.click(FOLDERS_COLLECTION);
    }

    async collection2() {
        return await this.click(ENVELOPES_COLLECTION);
    }

    async collection3() {
        return await this.click(INK_AND_TONER_COLLECTION);
    }

    async allCollectionsLink() {
        return await this.click(ALL_COLLECTIONS);
    }

    async loyaltyClose () {
        await this.waitUntilDisplayed(LL_POPUP);
        await this.click(LL_CLOSE);
        await this.waitForElementClose(LL_POPUP);
    }

    async logoClick() {
        await this.click(LOGO_LINK);
        await this.waitForTitle(PAGE_TITLE);
    }

}

module.exports = HomePage