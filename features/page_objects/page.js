//Base page class for POM inheritance
const {until} = require("selenium-webdriver");

class Page {

    constructor(driver) {
        this.driver = driver;
    }
    //Common page functions
    async open(url) {
        await this.driver.get(url);
    }

    find(locator) {
        return this.driver.findElement(locator);
    }

    async click(locator) {
        await this.find(locator).click();
    }

    async type(locator, inputText) {
        await this.find(locator).sendKeys(inputText);
    }

    async text(locator) {
        return this.find(locator).getText();
    }

    async value(locator) {
        return this.find(locator).getAttribute('value');
    }

    async waitForTitle(expectedTitle) {
        this.driver.wait(until.titleIs(expectedTitle), 10000);
    }

    async waitForElementClose(element) {
        this.driver.wait(until.elementIsNotVisible(this.find(element)), 10000);
    }

    async waitUntilDisplayed(element) {
        //this.driver.wait(until.elementIsVisible(this.find(element)), 10000);
        await this.find(element).isDisplayed();
    }

   /*async isDisplayed(locator, timeout) {
        if (timeout) {
            await this.driver.wait(until.elementLocated(locator), timeout);
            await this.driver.wait(until.elementIsVisible(this.find(locator)), timeout);
            return true;
        } else {
            try {
                await this.find(locator).isDisplayed();
            } catch (error) {
                return false;
            }
        }
    }*/

    async getAlert() {
        return this.driver.switchTo().alert();
    }

    async alertText() {
        await this.getAlert();
        return await this.driver.getText();
    }

    async refreshPage() {
        return this.driver.navigate().refresh();
    }

    async returnFromDestination() {
        return this.driver.navigate().back();
    }

    async maximize() {
        return this.driver.manage().window().maximize();
    }

}

module.exports = Page