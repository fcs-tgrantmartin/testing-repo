const LoginPage = require('./login.page')

const HEART ={xpath:'//*[@id="shopify-section-product-template"]/section/div/div[2]/div/div[2]/div/div[1]/div[2]/div[1]/div[1]/div/div'}
const ADD_TO_CART_BUTTON = {xpath:'//*[@id="product_form_1987503521850"]/div[4]/button'}
const QTY_UP_BUTTON = {xpath:'//*[@id="product_form_1987503521850"]/div[3]/div/div/div/button[2]'}
//const QTY_DOWN_BUTTON = {xpath:'//*[@id="product_form_1987503521850"]/div[3]/div/div/div/button[1]'}
const PRODUCT_QTY = {xpath:'/html/body/main/div[1]/section/div/div[2]/div/div[2]/div/div[1]/div[2]/form/div[2]/div/div/div/input'}

class ProductPage extends LoginPage {

    constructor(driver) {
        super(driver)
    }
    async favorite(){
        await this.click(HEART);
    }

    async addToCart (number){
        number = number || 1;
        if(number > 1)
        {
            for(let i = 1; i < number; i++)
            {
                await this.click(QTY_UP_BUTTON);
            }
        }
        await (number === await this.value(PRODUCT_QTY));
        return await this.click(ADD_TO_CART_BUTTON);
    }

}

module.exports = ProductPage