const LoginPage = require('./login.page')

const TOP_LEFT_PRODUCT = {xpath: '/html/body/main/div[1]/div/div[4]/div/ul/li[1]'}
const TOP_MIDDLE_PRODUCT = {xpath:'/html/body/main/div[1]/div/div[4]/div/ul/li[2]'}
const TOP_RIGHT_PRODUCT = {xpath: '/html/body/main/div[1]/div/div[4]/div/ul/li[3]'}
const PRODUCTS_IN_COLLECTION = {xpath:'/html/body/main/div[1]/div/div[1]/div[1]/b'}


class CollectionPage extends LoginPage {


    constructor(driver) {
        super(driver);
    }
    async product1() {
        await this.click(TOP_LEFT_PRODUCT);
    }

    async product2() {
        await this.click(TOP_MIDDLE_PRODUCT);
    }

    async product3() {
        await this.click(TOP_RIGHT_PRODUCT);
    }

    async collectionMembers() {
        return this.text(PRODUCTS_IN_COLLECTION);
    }

}

module.exports = CollectionPage