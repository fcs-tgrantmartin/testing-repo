Feature: Remove Address From Account
  @TEST_QAT-237
  Scenario: Removing Address From Account
    Given Logged in to website as a customer
    And Account has more than one Address
    When Click on Addresses
    And Click Delete Address
    Then Address is Removed