#!/bin/bash

for file in `find ./features -type f -name "*.feature"`;
do
npm run test -- ${file};
export token=$(curl -H "Content-Type: application/json" -X POST --data "{ \"client_id\": \"$client_id\",\"client_secret\": \"$client_secret\" }" https://xray.cloud.xpand-it.com/api/v1/authenticate| tr -d '"');
curl -H "Content-Type: application/json" -X POST -H "Authorization: Bearer $token"  --data @"data.json" https://xray.cloud.xpand-it.com/api/v1/import/execution/cucumber;
done;